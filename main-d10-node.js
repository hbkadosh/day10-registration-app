// main-D10-node.js

var express = require("express");
var path = require("path");

var app = express();

// initialization -------------------------------------------------------------------------
var allRegistrations = [];

var placeRegistration = function(registrationDetails){
    console.log(registrationDetails);
}

// routes ---------------------------------------------------------------------------------
app.get("/register", function(req, res) { // user at client placing registration

    allRegistrations.push(req.query);
    
    console.log("All registrations:\n %s", JSON.stringify(allRegistrations));

    res.status(201).end();
});  

app.get("/queryRegister", function(req, res) { // Administrator at Express clt console querying regns
    console.log("... registrations captured...");
    res.status(200);
    res.type("application/json");
    res.json(allRegistrations);
})

app.use(express.static(path.join(__dirname, "public")));

app.use("/libs", express.static(path.join(__dirname, "bower_components")));


// set port  ---------------------------------------------------------------------------
app.set("port", parseInt(process.argv[2]) || 3000);


// start Node server
app.listen(app.get("port"), function() {
    console.log("%s : App main-d10-node started at port %d", new Date(), app.get("port"));
});


// end

