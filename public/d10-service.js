// d10-service.js - for Service injection
(function() { // no Controller needed if UI not involved

    var Registration = angular.module("Registration",[]);

    var RegistrationSvc = function($http) {

        var registrationSvc = this;

        this.register = function(registrationDetail) {

            console.log("d10 SVC: inside Registration Service register() - from person");
            console.log("d10 SVC: registrationDetail: ", registrationDetail);

            return ($http.get("/register", {
                params: registrationDetail  
            }));
        }


        registrationSvc.getRegistration = function() {

            console.log("d10 SVC: inside Registration Service getRegistration - from Express console");

            return ($http.get("/queryRegister")
                .then(function(result) {
                    console.log("d10 SVC: ", result);
                    return (result.data);
                }));
        }
    };

    // define a Service
    Registration.service("RegistrationSvc", [ "$http", RegistrationSvc]);

}) ();





