// d10 express-console.js (query registration details)

(function() {

    var RegConsoleApp = angular.module("RegConsoleApp", ["Registration"]); // link to 2nd http form

    var RegConsoleCtrl = function(RegistrationSvc) {
        var regConsoleCtrl = this;

        regConsoleCtrl.registrations = [];

        regConsoleCtrl.getRegistration = function() {

            RegistrationSvc.getRegistration()
                .then(function(allRegistrations) {
                    regConsoleCtrl.registrations = allRegistrations;
                    console.log("Express CTRL: %s", allRegistrations);
                })
        }

        regConsoleCtrl.getRegistration();
    };

    RegConsoleCtrl.$inject = [ "RegistrationSvc" ];

    // specify this Controller in ng-application in the HTML form for summary view
    RegConsoleApp.controller("RegConsoleCtrl", [ "RegistrationSvc", RegConsoleCtrl ]);

}) ();


