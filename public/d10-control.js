// d10-control.js (interface btw Registration index.html with Express & Registration Service)

// (functio() {}) ();  IIFE

(function() {
    // everything inside this function is our application
    // create an instance of Angular app/module
    var RegistrationApp = angular.module("RegistrationApp", ["Registration"]); // 1st para: App name, 2nd para: App dependencies
    
    // every Angular application must have at least one Controller
    var RegistrationCtrl = function(RegistrationSvc) { // link RegistrationSvc into RegistrationCtrl
        var registrationCtrl = this;

        var initForm= function(registrationCtrl) { // form data
            registrationCtrl.name = "";
            registrationCtrl.gender = "";
            registrationCtrl.dob = "";
            registrationCtrl.address = ""; 
            registrationCtrl.country = "";
            registrationCtrl.contact = "";
            registrationCtrl.email = ""; 
            registrationCtrl.password = "";
        }

   
    var registrationQueryObject = function(registrationCtrl) {
        return({
            name: registrationCtrl.name,
            gender: registrationCtrl.gender,
            dob: registrationCtrl.dob,
            address: registrationCtrl.address, 
            country: registrationCtrl.country,
            contact: registrationCtrl.contact,
            email: registrationCtrl.email, 
            password: registrationCtrl.password   
        })
    }

    initForm(registrationCtrl);

    // using FlowerSvc ($http) to send data to server
    registrationCtrl.register = function() {

        console.log("d10 CTRL: inside registrationCtrl.register()...");

        RegistrationSvc.register(registrationQueryObject(registrationCtrl))
            .then(function() {
                window.alert("Your registration is successful! Thank you.");
                console.log("d10 CTRL: QueryObj > %s", registrationCtrl);
                initForm(registrationCtrl);
            });
            
    }
}    

    // define 1st controller 
    RegistrationApp.controller("RegistrationCtrl", [ "RegistrationSvc", RegistrationCtrl ]);
    // controller is always a function
    // RegistrationApp.controller("RegistrationCtrl", [ "$http", "$log", "RegistrationSvc", RegistrationCtrl ]); 

}) ();








